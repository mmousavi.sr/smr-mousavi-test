#!/bin/bash

set -e

# Build the Docker image
docker build -t hello-world .

# Run the container in the background
docker run -d --name hello-world -p 8080:8080 hello-world

# Wait for the container to start
sleep 20

# Test the application
curl localhost:8080

# Check the Prometheus metrics endpoint
curl localhost:8080/metrics | grep http_requests_total