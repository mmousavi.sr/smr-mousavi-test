# smr-mousavi-test



### Instructions

# Sample Application Deployment and Testing with Vagrant and GitLab CI

This repository contains a Vagrantfile and GitLab CI pipeline that automate the deployment and testing of a sample application. The Vagrantfile sets up a virtual machine with Docker, Minikube, and the sample application, while the GitLab CI pipeline builds and tests the application using Docker and Prometheus.

## Vagrantfile

The Vagrantfile does the following:

- Sets the base box to generic/ubuntu2004
- Configures the VM to have 2 CPU cores and 2048 MB of memory
- Installs Docker and Minikube
- Starts Minikube with the none driver (i.e., no VM is created, and Minikube uses the host machine's Docker daemon)
- Adds the stable Helm chart repository and updates it
- Deploys a sample application using kubectl and exposes it using a LoadBalancer service
- Installs Python and the required packages from requirements.txt
- Copies the script.sh script to the VM and makes it executable
- Sets up a synced folder to share files between the host and guest machines
- Runs the script.sh script on every `vagrant up` command

The `script.sh` script builds and tests the sample application, and exposes a single metric in Prometheus format for the amount of HTTP calls. The script builds a Docker image for the hello-world application, runs a container from the image, waits for the container to start, sends an HTTP request to the application, and checks the Prometheus metrics endpoint for the `http_requests_total` metric.

## GitLab CI Pipeline

The GitLab CI pipeline does the following:

- Defines the Docker image to use as the base image for the pipeline, and enables the Docker-in-Docker service
- Sets environment variables for the Docker driver and the container image name
- Defines two stages for the pipeline: build and test
- In the build stage, builds the Docker image, tags it with the GitLab registry URL and the commit SHA, and pushes it to the GitLab registry
- In the test stage, runs the Docker container from the GitLab registry, waits for it to start, sends an HTTP request to the application, and checks the Prometheus metrics endpoint for the `http_requests_total` metric.

Regarding design choices, Minikube was chosen to provision a single node Kubernetes cluster since it is easy to set up and use for development and testing purposes. Helm was used to deploy the sample application since it simplifies the process of managing Kubernetes manifests and allows for easy versioning and rollbacks. GitLab CI was chosen as the CI/CD tool as it integrates well with GitLab repositories. A single metric called `http_requests_total` in Prometheus format was added to track the number of HTTP requests handled by the application, which can be scraped by a Prometheus server and used for monitoring and alerting purposes.